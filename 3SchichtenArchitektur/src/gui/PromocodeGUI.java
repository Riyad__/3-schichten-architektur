package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import java.awt.Component;
import javax.swing.Box;  

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblPromocode;

	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setBackground(Color.LIGHT_GRAY);
		setTitle("PromotionCodeGenerator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.GRAY);
		contentPane.add(panel, BorderLayout.SOUTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{30, 150, 30, 0};
		gbl_panel.rowHeights = new int[]{0, 23, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 1;
		gbc_verticalStrut.gridy = 0;
		panel.add(verticalStrut, gbc_verticalStrut);
		
		JButton btnNewPromoCode = new JButton("Generiere neuen Promotioncode");
		GridBagConstraints gbc_btnNewPromoCode = new GridBagConstraints();
		gbc_btnNewPromoCode.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewPromoCode.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewPromoCode.gridx = 1;
		gbc_btnNewPromoCode.gridy = 1;
		panel.add(btnNewPromoCode, gbc_btnNewPromoCode);
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_1 = new GridBagConstraints();
		gbc_verticalStrut_1.insets = new Insets(0, 0, 0, 5);
		gbc_verticalStrut_1.gridx = 1;
		gbc_verticalStrut_1.gridy = 4;
		panel.add(verticalStrut_1, gbc_verticalStrut_1);
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code = logic.getNewPromoCode();
				lblPromocode.setText(code);
			}
		});
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.LIGHT_GRAY);
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		lblPromocode = new JLabel("");
		panel_1.add(lblPromocode);
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.GRAY);
		contentPane.add(panel_2, BorderLayout.NORTH);
		
		JLabel lblBeschriftungPromocode = new JLabel("Promotioncode f�r 2-fach IT$ in einer Stunde");
		lblBeschriftungPromocode.setForeground(Color.WHITE);
		panel_2.add(lblBeschriftungPromocode);
		lblBeschriftungPromocode.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		lblBeschriftungPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.GRAY);
		contentPane.add(panel_3, BorderLayout.WEST);
		
		JLabel lblPromocode_1 = new JLabel("Promocode:");
		lblPromocode_1.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		lblPromocode_1.setForeground(Color.WHITE);
		panel_3.add(lblPromocode_1);
		this.setVisible(true);
	}

}

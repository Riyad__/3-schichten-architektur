package start;

import data.IPromoCodeData;
import data.PromoCodeDataStub;
import gui.PromocodeGUI;
import gui.PromocodeTUI;
import logic.IPromoCodeLogic;
import logic.PromoCodeLogicStub;

public class PromoCodeStart {

	public static void main(String[] args) {
		IPromoCodeData data = new PromoCodeDataStub();
		IPromoCodeLogic logic = new PromoCodeLogicStub(data);

		new PromocodeGUI(logic);	//die GUI muss vor dem TUI gestartes werden, da sonst gewartet wird, bis das TUI beendet wird
		new PromocodeTUI(logic);
	
	}

}

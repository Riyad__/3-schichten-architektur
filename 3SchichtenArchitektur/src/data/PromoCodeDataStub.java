package data;

import java.io.*;
import java.util.Scanner;

public class PromoCodeDataStub implements IPromoCodeData {
	File file = new File("promocodes.txt");

	@Override
	public boolean savePromoCode(String code) {
		try (FileWriter out = new FileWriter(file, true)) {
			out.append(code + '\n');
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e1) {
			e1.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean isPromoCode(String code) {
		try (Scanner scan = new Scanner(file)) {
			while (scan.hasNextLine())
				if (scan.nextLine().equals(code)) {
					return true;
				}
			;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}

}